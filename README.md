# RPi0W images

The images provided by this flake may be migrated to my general nix config later. If this happens, I'll add a link to it here.

To build the image:

`nix build .#images.rpi0w.<IMAGE>`
