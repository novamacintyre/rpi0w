{
  description = "Metaflake for Raspberry Pi 0 W images";
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-23.11";

  outputs = { self, nixpkgs, ... }@inputs: rec {
    nixosConfigurations = {
      base = nixpkgs.lib.nixosSystem {
        system = "armv6l-linux";
        modules = [
          "${nixpkgs}/nixos/modules/installer/sd-card/sd-image-raspberrypi.nix"
          ./base.nix
        ];
      };
      cups-server = nixpkgs.lib.nixosSystem {
        system = "armv6l-linux";
        modules = [
          "${nixpkgs}/nixos/modules/installer/sd-card/sd-image-raspberrypi.nix"
          ./base.nix
          ./cups/base.nix
          ./cups/brother-hl-l2300d.nix
          ./machines/cleo.nix
          ./networking/base.nix
        ];
      };
    };
    images.rpi0w.base = nixosConfigurations.base.config.system.build.sdImage;
    images.rpi0w.cups-server = nixosConfigurations.cups-server.config.system.build.sdImage;
  };
}
