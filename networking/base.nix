{ lib, nixpkgs, ... }:

{
  networking.wireless.enable = true;
  networking.wireless.userControlled.enable = true;
  networking.wireless.networks."SSID".pskRaw = "PSK_RAW";
}
