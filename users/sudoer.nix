{ lib, nixpkgs, ... }:

{
  services.getty.autologinUser = "nixos";
  security.sudo.extraRules = [
    {
      users = [ "nixos" ];
      commands = [{
        command = "ALL";
        options = [ "SETENV" "NOPASSWD" ];
      }];
    }
  ];
  users.users.nixos = {
    isNormalUser = true;
    extraGroups = [
      "sudo"
      "wheel"
    ];
    group = "users";
    uid = 1000;
  };
}
