{ lib, nixpkgs, pkgs, ... }:

{
  hardware.printers = {
    ensurePrinters = [
      {
        name = "Brother_HL-L2300D";
        location = "Home";
        deviceUri = "usb://Brother/HL-L2300D%20series?serial=U63878H3N420296";
        model = "raw";  # raw queue
        ppdOptions = {
          Duplex = "DuplexTumble";
          PageSize = "Letter";
        };
      }
    ];
  };
}
