{ lib, nixpkgs, pkgs, ... }:

{

  environment.systemPackages = [ pkgs.ghostscript ];

  # Would love to make this part of the `let`
  nixpkgs.overlays = [
    (final: super: {
      cups-filters = super.cups-filters.overrideAttrs (finalAttrs: previousAttrs: {
        configureFlags = previousAttrs.configureFlags
          ++ [ "--with-cups-config=${pkgs.cups.dev}/bin/cups-config" ];
        nativeBuildInputs = previousAttrs.nativeBuildInputs ++ [ super.glib ];
      });
    })
  ];

  services.avahi.enable = true;
  services.avahi.nssmdns = true;
  services.avahi.openFirewall = true;
  services.avahi.publish.addresses = true;
  services.avahi.publish.domain = true;
  services.avahi.publish.hinfo = false;

  services.printing.allowFrom = [ "192.168.0.*" ];
  services.printing.browsing = true;
  services.printing.defaultShared = true;
  services.printing.enable = true;
  services.printing.listenAddresses = [ "0.0.0.0:631" ];
  services.printing.openFirewall = true;

}
