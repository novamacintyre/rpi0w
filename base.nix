{ lib, nixpkgs, ... }:

{
  boot.supportedFilesystems = lib.mkForce [ "vfat" "ext4" ];

  nixpkgs.buildPlatform.system = "x86_64-linux"; #If you build on x86 other wise changes this.
  nixpkgs.config.allowUnsupportedSystem = true;
  nixpkgs.hostPlatform.system = "armv6l-linux";

  # https://github.com/NixOS/nixpkgs/issues/154163#issuecomment-1350599022
  nixpkgs.overlays = [
    (final: super: {
      # Due to regressions in llvmPackages_15
      llvmPackages = super.llvmPackages_14;
      makeModulesClosure = x:
        super.makeModulesClosure (x // { allowMissing = true; });
    })
  ];
  system.stateVersion = "23.11";
}
